export const prodFontTheme = {
  fontFamily: {
    main: "Inter_900Black",
  },
  fontSize: {
    xxl: 144,
    xl: 36,
    large: 24,
    mediumLarge: 21,
    medium: 18,
    mediumSmall: 16,
    small: 14,
    ittybitty: 11,
  },
  fontWeight: {
    black: 900,
    bold: 700,
    normal: 400,
    light: 300,
  },
};
