import React, { useContext, useEffect, useState } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import { moderateScale, verticalScale } from "react-native-size-matters";
import styled from "styled-components";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import { MusicPlayerContext } from "../model/MusicPlayerContext";
import { get } from "lodash";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { api_endpoints } from "../api/endpoints";
import SwipeCardComponent from "./SwipeCardComponent";
import EstablishmentProfileComponent from "./EstablishmentProfileComponent";
const testArtwork = require("../assets/test_artwork_cover.png");

const WrapperContainer = styled.View`
  width: 100%;
  height: ${verticalScale(80)};
  background-color: #f8f8f8;
  opacity: 0.95;
  position: absolute;
  bottom: 0;
`;

const StatusContainer = styled.View`
  width: 100%;
  height: ${verticalScale(15)};
  background-color: #b9bfc6;
  padding: 3px 0px 0px 8px;
`;

const StatusText = styled.Text`
  font-size: 12px;
`;

const MusicPlayerContainer = styled.View`
  height: 100%;
  padding: 5px 5px 5px 8px;
`;
const TrackContainerViewStyled = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 5px;
`;

const AlbumArtworkContainerStyled = styled.View`
  width: 50px;
`;

const AlbumArtworkStyled = styled(Image)`
  height: 50px;
  width: 50px;
`;
const SongInfoContainerStyled = styled.View`
  width: 50%;

  align-self: center;
  margin-left: 5px;
`;

const SongNameTextStyled = styled.Text`
  padding: 2px;
`;
const ArtistNameTextStyled = styled.Text`
  padding: 2px;
`;

const SongActionButtonsContainerStyled = styled.View`
  width: 20%;

  flex-direction: row;
  margin-left: ${moderateScale(50)};
  justify-content: space-evenly;
  align-self: center;
`;
const PlayButtonStyled = styled(FontAwesome5)``;
const PlayButton = ({ buttonSize, trackURI, track }) => {
  const [isPlaying, setIsPlaying] = useState(true);
  // const { playSong } = useContext(MusicPlayerContext);
  const { playSong, pauseSong } = useContext(MusicPlayerContext);
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          if (isPlaying) {
            setIsPlaying(false);
            pauseSong();
          } else {
            setIsPlaying(true);
            playSong({ trackURI, track });
          }
        }}
        style={{
          alignSelf: "center",
          marginRight: 10,
        }}
      >
        <PlayButtonStyled
          name={isPlaying ? "pause" : "play"}
          size={buttonSize}
          color={isPlaying ? "#050405" : "black"}
        />
      </TouchableOpacity>
    </>
  );
};

const FavoriteButton = ({ buttonSize, trackURI, trackId }) => {
  const {
    favoriteSong,
    unFavoriteSong,
    track,
    currentTrackSaveStatus,
    saveTrackFunc,
  } = useContext(MusicPlayerContext);

  useEffect(async () => {
    const url =
      api_endpoints.spotify.get_saved_tracks + "?track_ids_list=" + trackId;

    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: url,
    });

    console.log("RESULTS", results);
    if (results) {
      setIsFavorited(get(results, "tracks_favorite_status"));
    }
  }, [track]);

  return (
    <>
      <TouchableOpacity
        onPress={() => {
          if (currentTrackSaveStatus) {
            saveTrackFunc(false);
            unFavoriteSong({ trackURI });
          } else {
            saveTrackFunc(true);
            favoriteSong({ trackURI });
          }
        }}
        style={{
          alignSelf: "center",
          marginRight: 30,
        }}
      >
        <AntDesign
          name={currentTrackSaveStatus ? "heart" : "hearto"}
          size={buttonSize}
          color={currentTrackSaveStatus ? "pink" : "black"}
        />
      </TouchableOpacity>
    </>
  );
};

const SkipButton = ({ buttonSize }) => {
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          console.log("skipping to next track ");
        }}
        style={{
          alignSelf: "center",
          marginRight: 10,
        }}
      >
        <FontAwesome5
          name="angle-double-right"
          size={buttonSize}
          color="black"
        />
      </TouchableOpacity>
    </>
  );
};

const Track = ({ track }) => {
  return (
    <>
      <TrackContainerViewStyled>
        <AlbumArtworkContainerStyled>
          <AlbumArtworkStyled
            source={{ uri: get(track, "track.album.images[0].url") }}
          />
        </AlbumArtworkContainerStyled>
        <SongInfoContainerStyled>
          <SongNameTextStyled>{get(track, "track.name")}</SongNameTextStyled>
          <ArtistNameTextStyled>
            {get(track, "track.artists[0].name")}
          </ArtistNameTextStyled>
        </SongInfoContainerStyled>
        <SongActionButtonsContainerStyled>
          {/* <FavoriteButton
            buttonSize={24}
            trackURI={get(track, "track.uri")}
            trackId={get(track, "track.id")}
          /> */}
          {/* <PlayButton
            buttonSize={21}
            trackURI={get(track, "track.uri")}
            track={track}
          /> */}
          {/* <SkipButton buttonSize={26} /> */}
        </SongActionButtonsContainerStyled>
      </TrackContainerViewStyled>
    </>
  );
};

export default function ({ track, establishment }) {
  const [isShowingEstablishment, setIsShowingEstablishment] = useState(false);
  return (
    <>
      <WrapperContainer>
        <TouchableOpacity
          onPress={() => {
            setIsShowingEstablishment(true);
          }}
        >
          <StatusContainer>
            <StatusText>
              {establishment.name} • {establishment.address}
            </StatusText>
          </StatusContainer>
        </TouchableOpacity>
        <MusicPlayerContainer>
          <Track track={track} />
        </MusicPlayerContainer>
      </WrapperContainer>
      {isShowingEstablishment ? (
        <SwipeCardComponent setIsSwipeCardShowing={setIsShowingEstablishment}>
          <EstablishmentProfileComponent
            establishment={establishment}
            showPlayButton={false}
          />
        </SwipeCardComponent>
      ) : (
        <></>
      )}
    </>
  );
}
