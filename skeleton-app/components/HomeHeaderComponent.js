import React, { useEffect, useState } from "react";
import { Text, View, Dimensions, TouchableOpacity } from "react-native";
import styled from "styled-components/native";
import { moderateScale, verticalScale } from "react-native-size-matters";

console.log("width", Dimensions.get("window").width);
const HeaderContainerStyled = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-evenly;
`;

const HeaderSectionContainerStyled = styled.View``;

const TitleContainerStyled = styled.View`
  justify-content: flex-end;
  margin-left: ${moderateScale(85)};
  margin-top: 10px;
  margin-bottom: 0px;
`;

const TitleTextStyled = styled.Text`
  font-size: 18px;
  margin-right: ${moderateScale(30)};
`;

const DividerContainerStyled = styled.View`
  height: 4px;
  margin-top: ${verticalScale(7.5)};
  width: ${Dimensions.get("window").width / 2 + 8};
`;

const HeaderSection = (params) => {
  return (
    <>
      <TouchableOpacity
        onPress={() => {
          params.setShowing(params.title);
        }}
      >
        <HeaderSectionContainerStyled>
          <TitleContainerStyled>
            <TitleTextStyled>{params.title}</TitleTextStyled>
          </TitleContainerStyled>
          <DividerContainerStyled style={{ backgroundColor: params.color }} />
        </HeaderSectionContainerStyled>
      </TouchableOpacity>
    </>
  );
};
export default function ({ show }) {
  const [showing, setShowing] = useState("Map");

  useEffect(() => {
    if (show) {
      setShowing(show);
    }
  }, []);
  return (
    <>
      <HeaderContainerStyled>
        {showing == "Map" ? (
          <>
            <HeaderSection
              title={"Map"}
              color={"#050405"}
              setShowing={setShowing}
            />

            <HeaderSection
              title={"List"}
              color={"#B9BFC6"}
              setShowing={setShowing}
            />
          </>
        ) : (
          <>
            <HeaderSection
              title={"Map"}
              color={"#B9BFC6"}
              setShowing={setShowing}
            />
            <HeaderSection
              title={"List"}
              color={"#050405"}
              setShowing={setShowing}
            />
          </>
        )}
      </HeaderContainerStyled>
    </>
  );
}
