import React from "react";
import styled from "styled-components/native";
import { verticalScale } from "react-native-size-matters";
import { List } from "react-native-paper";
import { ScrollView, TouchableOpacity } from "react-native";
import { EstablishmentCard } from "./EstablishmentProfileComponent";
const ListSectionStyled = styled(List.Section)`
  background-color: white;
  height: 100%;
`;

const ListItemStyled = styled(List.Item)`
  padding-top: 0px;
  height: ${verticalScale(35)};
  background-color: white;
`;

const ResultContainerStyled = styled.View`
  padding: 5px 10px 20px 15px;
`;

const ScrollViewStyled = styled(ScrollView)``;

export default function ({ establishments, navigation }) {
  return (
    <ListSectionStyled>
      <ScrollViewStyled>
        <ResultContainerStyled>
          {establishments.map((location) => {
            return (
              <EstablishmentCard
                key={location.id}
                establishment={location}
                navigation={navigation}
              />
            );
          })}
        </ResultContainerStyled>
      </ScrollViewStyled>
    </ListSectionStyled>
  );
}
