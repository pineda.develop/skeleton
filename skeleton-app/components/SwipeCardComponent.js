import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Alert,
} from "react-native";
import styled from "styled-components/native";
import { moderateScale, verticalScale } from "react-native-size-matters";
import {
  PanGestureHandlerEventPayload,
  Gesture,
  GestureDetector,
  PanGesture,
  TapGesture,
  PanGestureHandler,
  State,
  LongPressGestureHandler,
} from "react-native-gesture-handler";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  useAnimatedGestureHandler,
  withTiming,
  withDecay,
  Easing,
  runOnJS,
} from "react-native-reanimated";
import EstablishmentProfileComponent from "./EstablishmentProfileComponent";
import { Divider } from "react-native-elements";
import MusicPlayer from "./MusicPlayer";
import { AntDesign } from "@expo/vector-icons";

const image = require("../assets/images/landing_screen.jpg");
const phoneWidth = Dimensions.get("window").width;
const phoneHeight = Dimensions.get("window").height;

const BackgroundContainerStyled = styled.View`
  flex: 1;
  background-color: green;
  width: ${phoneWidth};
  height: ${phoneHeight};
`;

const ContainerStyled = styled.View`
  position: absolute;
  bottom: ${verticalScale(150)};
  padding: 10px;
  width: 100%;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  background-color: white;
`;
const SwiperHandlerContainerStyled = styled.View`
  width: 100%;
  flex-direction: row;
  padding-right: 10px;
`;
const ExitButtonContainerStyled = styled.View`
  align-self: flex-end;
`;

const DividerContainerStyled = styled.View`
  /* width: ${phoneWidth}; */
  padding-left: ${moderateScale(148)};
  align-self: center;
  width: 95%;
  /* margin: 0 auto; */
`;
const DividerStyled = styled.View`
  background-color: #757a85;
  border-radius: 80px;
  height: 8px;
  width: ${moderateScale(80)};
`;

export default function ({ children, isPlayerShowing, setIsSwipeCardShowing }) {
  const expectedContainerHeight = isPlayerShowing ? 800 : 600;
  const [ContainerHeight, setContainerHeight] = useState(
    verticalScale(expectedContainerHeight)
  );
  const isPressed = useSharedValue(false);
  const isFullHeight = useSharedValue(false);
  const y = useSharedValue(500);
  const velocityY = useSharedValue(0);

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startY = y.value;
      ctx.startVelocityY = velocityY.value;
      isPressed.value = true;
    },
    onActive: (event, ctx) => {
      // Controls the swipe card size when swiping up and swiping down
      // Dependent on the distance the user swiped on the y-axis (event.translationY)

      y.value = ctx.startY + event.translationY;
      velocityY.value = ctx.startVelocityY + event.velocityY;
      // velocityY.value =
      isPressed.value = true;
    },
    onEnd: (event, ctx) => {},
    onFinish: (event) => {
      isPressed.value = false;
      console.log("EVENT", event);
      // The sizing of the container is not actually changed but rather the y-axis placement of the container
      if (event.translationY > 0) {
        //   Move the component down and only show the establishment card
        const toValue = 600;
        y.value = withTiming(toValue, {
          duration: 500,
          easing: Easing.bezier(0.25, 0.1, 0.25, 1),
        });
        isFullHeight.value = false;
      } else {
        //   Move the component up and show the large screen
        const toValue = 300;
        y.value = withTiming(toValue, {
          duration: 500,
          easing: Easing.bezier(0.25, 0.1, 0.25, 1),
        });
        isFullHeight.value = true;
      }
    },
  });

  const _handleStateChange = ({ nativeEvent }) => {
    if (nativeEvent.state === State.ACTIVE) {
      console.log("Testing Nested");
    }
  };

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: y.value }],
    };
  });

  return (
    <>
      <PanGestureHandler onGestureEvent={gestureHandler} minDistance={10}>
        <Animated.View style={[animatedStyle]}>
          <ContainerStyled style={{ height: ContainerHeight }}>
            <SwiperHandlerContainerStyled>
              <DividerContainerStyled>
                <DividerStyled />
              </DividerContainerStyled>
              <ExitButtonContainerStyled>
                <TouchableOpacity
                  onPress={() => {
                    setIsSwipeCardShowing(false);
                  }}
                >
                  <AntDesign name="closecircleo" size={24} color="#757a85" />
                </TouchableOpacity>
              </ExitButtonContainerStyled>
            </SwiperHandlerContainerStyled>
            {children}
          </ContainerStyled>
        </Animated.View>
      </PanGestureHandler>
    </>
  );
}
