import { Avatar } from "react-native-paper";
import React from "react";

export default function ({ color = "black", size = 100 }) {
  return <Avatar.Text size={size} style={{ backgroundColor: color }} />;
}
