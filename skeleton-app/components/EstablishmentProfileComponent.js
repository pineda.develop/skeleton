import { moderateScale, verticalScale } from "react-native-size-matters";
import Swiper from "react-native-swiper";
import React, {
  useCallback,
  memo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";
import {
  FlatList,
  View,
  Dimensions,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import styled from "styled-components/native";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import { Carousel } from "@ant-design/react-native";
import { MusicPlayerContext } from "../model/MusicPlayerContext";
import { api_endpoints } from "../api/endpoints";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { get, map } from "lodash";

const { width: windowWidth, height: windowHeight } = Dimensions.get("window");

const image = require("../assets/images/landing_screen.jpg");
const source = {
  src: "https://source.unsplash.com/1600x900/?bar",
  alt: "random unsplash image",
};
const testArtwork = require("../assets/test_artwork_cover.png");
const EstablishmentCardContainerStyled = styled.View`
  width: 100%;
  padding: 8px 2px 2px 2px;
`;

const EstablishmentImagesContainerStyled = styled.View`
  width: 100%;
  height: ${verticalScale(190)};
`;

const ImageStyled = styled(Image)`
  width: 100%;
  height: ${verticalScale(190)};
  border-radius: 12px;
`;

const DetailsContainerStyled = styled.View`
  width: 100%;
  /* height: ${verticalScale(50)}; */
  margin-top: 2px;
  flex-direction: row;
  justify-content: space-between;
  padding: 2px 10px 2px 0px;
`;

const LocationDetailsContainerStyled = styled.View`
  padding-top: 5px;
`;
const ActionButtonsContainerStyled = styled.View`
  justify-content: center;
  margin-right: 10px;
`;

const NameTextStyled = styled.Text`
  /* font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  letter-spacing: -0.3px; */
  font-size: 14px;
  color: #050405;
  height: ${verticalScale(20)};
  padding-top: 5px;
`;
const LocationTextStyled = styled.Text`
  font-size: 14px;
  color: #050405;
  height: ${verticalScale(20)};
  padding-top: 2px;
`;

const CarouselStyled = styled(Carousel)`
  width: 100%;
  height: ${verticalScale(190)};
`;

const PlayButtonStyled = styled(FontAwesome5)``;

export const EstablishmentCard = ({
  establishment,
  showPlayButton = false,
  navigation,
}) => {
  const [isPlaying, setIsPlaying] = useState(false);

  const onHorizontalSelectedIndexChange = (index) => {
    /* tslint:disable: no-console */
    console.log("horizontal change to", index);
  };
  const onVerticalSelectedIndexChange = (index) => {
    /* tslint:disable: no-console */
    console.log("vertical change to", index);
  };

  const address =
    establishment.address +
    " " +
    establishment.city +
    ", " +
    establishment.state;
  return (
    <>
      <EstablishmentCardContainerStyled>
        <EstablishmentImagesContainerStyled>
          {/* <ImageStyled source={image} resizeMode={"cover"} /> */}
          <CarouselStyled horizontal>
            {/* {get(establishment,['photos']?.map((photo, index) =>{
              return(    
                <>  <ImageStyled
                source={{
                  uri: "https://source.unsplash.com/random/?restaurant,",
                }}
                resizeMode={"cover"}
              />
              </> )
            })) */}

            <ImageStyled
              source={{
                uri: "https://source.unsplash.com/random/?restaurant,",
              }}
              resizeMode={"cover"}
            />
            <ImageStyled
              source={{
                uri: "https://source.unsplash.com/random/?club",
              }}
              resizeMode={"cover"}
            />
            <ImageStyled
              source={{
                uri: "https://source.unsplash.com/random/?nightlife",
              }}
              resizeMode={"cover"}
            />
          </CarouselStyled>
        </EstablishmentImagesContainerStyled>
        <DetailsContainerStyled>
          <TouchableOpacity
            onPress={() => {
              // Pass and merge params back to home screen
              navigation.navigate({
                name: "Home",
                params: { establishment: establishment },
                merge: true,
              });
            }}
            key={
              establishment.address + establishment.city + establishment.state
            }
          >
            <LocationDetailsContainerStyled>
              <NameTextStyled>{establishment.name}</NameTextStyled>
              <LocationTextStyled>{address}</LocationTextStyled>
            </LocationDetailsContainerStyled>
          </TouchableOpacity>
          <ActionButtonsContainerStyled>
            <TouchableOpacity
              onPress={() => {
                if (isPlaying) {
                  setIsPlaying(false);
                } else {
                  setIsPlaying(true);
                }
              }}
            >
              {showPlayButton ? (
                <PlayButtonStyled
                  name={isPlaying ? "pause" : "play"}
                  size={20}
                  color={isPlaying ? "red" : "black"}
                />
              ) : (
                <></>
              )}
            </TouchableOpacity>
          </ActionButtonsContainerStyled>
        </DetailsContainerStyled>
      </EstablishmentCardContainerStyled>
    </>
  );
};

const TrackContainerViewStyled = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 3px 2px 2px 2px;
  margin-top: 8px;
`;

const AlbumArtworkContainerStyled = styled.View`
  width: 60px;
`;

const AlbumArtworkStyled = styled(Image)`
  height: 60px;
  width: 60px;
`;
const SongInfoContainerStyled = styled.View`
  width: 50%;
  align-self: center;
  margin-left: 5px;
`;

const SongNameTextStyled = styled.Text`
  font-size: 14px;
  padding: 2px;
`;
const ArtistNameTextStyled = styled.Text`
  font-size: 14px;
  padding: 2px;
`;

const SongActionButtonsContainerStyled = styled.View`
  width: 20%;

  flex-direction: row;
  margin-left: ${moderateScale(50)};
  justify-content: space-evenly;
  align-self: center;
`;

const Track = ({
  songName,
  artistName,
  coverImageSource,
  trackSavedStatus,
  trackId,
  trackURI,
  track,
  establishment,
}) => {
  const [isSavedTrack, setSavedTrack] = useState(trackSavedStatus);
  const {
    unFavoriteSong,
    favoriteSong,
    playSong,
    setCurrentEstablishment,
    syncSaveControls,
  } = useContext(MusicPlayerContext);

  return (
    <>
      <TrackContainerViewStyled>
        <AlbumArtworkContainerStyled>
          <AlbumArtworkStyled source={{ uri: coverImageSource }} />
        </AlbumArtworkContainerStyled>
        <SongInfoContainerStyled>
          <SongNameTextStyled>{songName}</SongNameTextStyled>
          <ArtistNameTextStyled>{artistName}</ArtistNameTextStyled>
        </SongInfoContainerStyled>
        <SongActionButtonsContainerStyled>
          <FontAwesome5 name="spotify" size={23} color="#1ED760" />
        </SongActionButtonsContainerStyled>
      </TrackContainerViewStyled>
    </>
  );
};

const TrackListScrollViewStyled = styled(ScrollView)`
  flex: 1;
`;

const TrackList = ({ tracks, establishment }) => {
  return (
    <>
      {tracks &&
        tracks.map((track, index) => {
          return (
            <Track
              key={get(track, "track.id")}
              songName={get(track, "track.name")}
              artistName={get(track, "track.artists[0].name")}
              coverImageSource={get(track, "track.album.images[0].url")}
              trackId={get(track, "track.id")}
              trackURI={get(track, "track.uri")}
              track={track}
              establishment={establishment}
            />
          );
        })}
    </>
  );
};

const UpdateStatus = () => {
  return (
    <>
      <View style={{ marginTop: 5, marginBottom: 5, paddingLeft: 10 }}>
        <Text>Last Updated: 4 hours ago</Text>
      </View>
    </>
  );
};

export default function ({ establishment, showPlayButton = false }) {
  const [trackList, setTrackList] = useState();
  const { isPlayerVisible } = useContext(MusicPlayerContext);

  useEffect(async () => {
    const url =
      api_endpoints.spotify.get_playlist +
      "?playlist_id=" +
      establishment.playlist_id;
    console.log("establishment.playlist_id", establishment.playlist_id);

    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: url,
    });

    if (results) {
      setTrackList(results.tracks);
    }
  }, [establishment]);

  return (
    <>
      <TrackListScrollViewStyled
        bonuces={false}
        contentContainerStyle={{
          paddingBottom: isPlayerVisible
            ? verticalScale(150)
            : verticalScale(300),
        }}
      >
        <EstablishmentCard
          establishment={establishment}
          showPlayButton={showPlayButton}
        />
        {/* <UpdateStatus /> */}
        <TrackList tracks={trackList} establishment={establishment} />
      </TrackListScrollViewStyled>
    </>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "#fff",
  },
  containerHorizontal: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 150,
  },
  containerVertical: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 150,
  },
  text: {
    color: "#fff",
    fontSize: 36,
  },
});
