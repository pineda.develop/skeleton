import { SafeAreaView } from "react-native-safe-area-context";
import React, { useState, useEffect, useContext } from "react";
import { Text, View, StatusBar, Pressable, Alert } from "react-native";
import styled from "styled-components/native";
import { verticalScale } from "react-native-size-matters";
import * as WebBrowser from "expo-web-browser";
import { api_endpoints } from "../api/endpoints";
import { useUser } from "../model/UserContext";
import { get } from "lodash";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { storeObjectData } from "../model/LocalStorage";
import { AuthContext } from "../App";
import { Ionicons } from "@expo/vector-icons";
import { Divider } from "react-native-paper";
import { FontAwesome5 } from "@expo/vector-icons";
import { MusicPlayerContext } from "../model/MusicPlayerContext";

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  background-color: white;
`;

const TopLevelContainerStyled = styled.View`
  flex: 1;
  background-color: white;
`;

const HeaderViewStyled = styled.View`
  flex-direction: column;
  justify-content: space-between;
  padding: 20px 10px 0px 10px;
`;

const TitleTextStyled = styled.Text`
  font-size: 30px;
  font-weight: 500;
  margin: 0px 0px 5px 12px;
`;

const SettingOptionsContainerViewStyled = styled.View`
  margin-top: 3px;
  align-self: center;
  flex-direction: column;
`;

const SettingOptionTextStyled = styled.Text`
  font-size: 22px;
  padding-left: 10px;
`;

const PressableContainerStyled = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  padding: 5px 5px 0px 5px;
  /* background-color: green; */
  height: ${verticalScale(30)};
`;
const IconContainerStyled = styled.View`
  align-self: center;
`;
const TitleViewContainerStyled = styled.View`
  align-self: center;
  width: 90%;
  text-align: left;
`;
const TouchArrowContainerStyled = styled.View`
  align-self: center;
  margin-right: 5px;
`;
const PressableSettingName = ({
  onPressFunc,
  activeColor,
  unActiveColor,
  settingName,
  iconName,
}) => {
  return (
    <Pressable
      onPress={() => {
        onPressFunc();
      }}
      style={({ pressed }) => [
        {
          backgroundColor: pressed ? activeColor : unActiveColor,

          opacity: 0.9,
          marginTop: 10,
        },
      ]}
    >
      <PressableContainerStyled>
        <IconContainerStyled>
          <FontAwesome5 name={iconName} size={22} color="black" />
        </IconContainerStyled>
        <TitleViewContainerStyled>
          <SettingOptionTextStyled>{settingName}</SettingOptionTextStyled>
        </TitleViewContainerStyled>
        {/* <TouchArrowContainerStyled>
          <FontAwesome5 name="chevron-right" size={22} color="black" />
        </TouchArrowContainerStyled> */}
      </PressableContainerStyled>
    </Pressable>
  );
};

export default function ({ route, navigation }) {
  const [isEnabled, setIsEnabled] = useState(false);
  const { user } = useUser();
  const [hasSpotifyAccount, setHasSpotifyAccount] = useState(false);
  const [userEmail, setUserEmail] = useState(null);
  const { hidePlayer } = useContext(MusicPlayerContext);
  const { signOut } = useContext(AuthContext);

  useEffect(() => {
    getUserEmail = get(user, "user.email");
    setUserEmail(getUserEmail);
  }, [user]);

  const StartSpotifyAuthProcess = async () => {
    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: api_endpoints.spotify.get_spotify_url,
    });

    if (results) {
      let webResult = await WebBrowser.openBrowserAsync(results.url);
      console.log("closed browser", webResult);
      AlertMessage("Spotify Account Added");
    }
  };

  const CheckForSpotifyAccount = async () => {
    // Will check the database to see if the user has a Spotify account
    //  Sets the hook for Spotify after completion

    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: api_endpoints.spotify.check_for_account,
    });

    if (results) {
      if (
        get(results, "has_account") === "true" ||
        get(results, "has_account") === "True"
      ) {
        SpotifyConnectedAlert();
      } else {
        SpotifyDisconnecedAlert();
      }
    }
  };

  const StartRemovingSpotifyAccountProcess = async () => {
    const [results, error] = await ZombieAPI({
      method: "POST",
      endpoint: api_endpoints.spotify.delete_account,
    });

    if (results) {
      if (get(results, "deleted") === "true") {
        user.spotify.has_account = "false";
        storeObjectData("solo_user", user);
        setHasSpotifyAccount(false);
        AlertMessage("Spotify Account Removed");
      } else {
        setHasSpotifyAccount(true);
      }
    }
  };

  const SpotifyConnectedAlert = () =>
    Alert.alert(
      "Disconnect Spotify Account",
      "Remove this Spotify account from Solo",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Remove",
          onPress: () => {
            StartRemovingSpotifyAccountProcess();
          },
          style: "destructive",
        },
      ]
    );

  const SpotifyDisconnecedAlert = () => {
    Alert.alert("Connect Spotify Account", "Add your Spotify account to Solo", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "Add",
        onPress: () => {
          StartSpotifyAuthProcess();
        },
      },
    ]);
  };

  const AlertMessage = (message) => {
    Alert.alert(message, "", [
      {
        text: "OK",
        onPress: () => console.log("Okay Thank you"),
      },
    ]);
  };

  const SignOutAlert = () => {
    Alert.alert("Sign Out", "Are you sure you want to sign out?", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "Sign Out",
        onPress: () => {
          hidePlayer();
          signOut();
        },
        style: "destructive",
      },
    ]);
  };

  return (
    <SafeAreaViewStyled>
      <StatusBar barStyle={"dark-content"} />
      <TopLevelContainerStyled>
        <HeaderViewStyled>
          <View>
            <TitleTextStyled>Account</TitleTextStyled>
          </View>
          <Divider />
        </HeaderViewStyled>
        <SettingOptionsContainerViewStyled>
          {/* <PressableSettingName
            onPressFunc={console.log}
            activeColor={"rgb(210, 230, 255)"}
            unActiveColor={"white"}
            settingName={"Bookmarks"}
            iconName={"bookmark"}
          /> */}

          <PressableSettingName
            onPressFunc={CheckForSpotifyAccount}
            activeColor={"rgb(210, 230, 255)"}
            unActiveColor={"white"}
            settingName={"Spotify Account"}
            iconName={"spotify"}
          />
          <PressableSettingName
            onPressFunc={SignOutAlert}
            activeColor={"rgb(210, 230, 255)"}
            unActiveColor={"white"}
            settingName={"Sign Out"}
            iconName={"sign-out-alt"}
          />
        </SettingOptionsContainerViewStyled>
      </TopLevelContainerStyled>
    </SafeAreaViewStyled>
  );
}
