import { SafeAreaView } from "react-native-safe-area-context";
import {
  Text,
  View,
  Keyboard,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  StatusBar,
} from "react-native";
import styled from "styled-components/native";
import React, { useEffect, useState } from "react";
import { verticalScale, moderateScale } from "react-native-size-matters";
import { TextInput, Button, IconButton } from "react-native-paper";
import { AuthContext } from "../App";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";

const image = require("../assets/images/landing_screen.jpg");

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const ImageBackgroundStyled = styled(ImageBackground)`
  width: ${Dimensions.get("window").width};
  height: ${Dimensions.get("window").height};
  background-color: black;
  opacity: 1;
`;

const LoginContainerStyled = styled.View`
  position: absolute;
  bottom: 0;
  /* 
    Height 320 with keyboard not showing 
    Height 600 with keyboard showing
   */

  background-color: white;
  padding: 10px;
  width: 100%;
  padding-top: 30px;
  border-radius: 20px;
`;

const FormInfoInputsContainerStyled = styled.View`
  padding: 10px;
  background-color: white;
`;

const FormInputStyled = styled(TextInput)`
  margin-top: 14px;
  background-color: white;
  border: 0;
`;

const LoginButtonContainer = styled.View`
  margin-top: ${verticalScale(10)};
`;

const ErrorMessageStyled = styled.Text`
  margin: 0 auto;
  color: red;
`;

const LoginButtonStyled = styled(Button)`
  width: ${moderateScale(354)};
  height: ${verticalScale(33)};
  background: #050405;
  box-shadow: 0px 1.5px 6px rgba(55, 65, 81, 0.08);
  border-radius: 4px;
  text-align: center;
  /* border: 1px solid #ffffff; */
  padding-top: ${verticalScale(5)};
  margin-top: 17px;
  margin-left: 5px;
`;

const GoToSignUpButtonStyled = styled(Button)`
  width: ${moderateScale(354)};
  height: ${verticalScale(33)};
  background: white;
  border-radius: 4px;
  text-align: center;
  /* border: 1px solid #ffffff; */
  padding-top: ${verticalScale(5)};
  margin-left: 5px;
`;

const TitleContainerStyled = styled.View`
  height: ${verticalScale(40)};
  background-color: white;
  margin-top: 15px;
`;

const TitleTextStyled = styled.Text`
  font-weight: 600;
  font-size: 36px;
  line-height: 20px;
  padding-top: 30px;
  padding-left: 10px;
`;

export default function ({ navigation }) {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [loginContainerHeight, setLoginContainerHeight] = useState(
    verticalScale(305)
  );

  const { signIn } = React.useContext(AuthContext);
  const smallContainer = verticalScale(300);
  const fullScreenContainer = verticalScale(500);

  const validateEmail = () => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      setErrorMessage(null);
    }
    setErrorMessage("Incorrect Email");
  };

  const submitLoginInfo = async () => {
    // Check to see if the user entered a password
    if (!password.trim()) {
      setErrorMessage("Please enter a password");
    }

    if (!email.trim()) {
      setErrorMessage("Please enter an email address");
    }

    // If the user has entered an email, password and a name then submit the information
    if (email && password) {
      signIn({ email, password, setErrorMessage });
    }
  };

  const onSwipeUp = (gestureState) => {
    setLoginContainerHeight(fullScreenContainer);
    emailInput.focus();
  };

  const onSwipeDown = (gestureState) => {
    Keyboard.dismiss();
    setLoginContainerHeight(smallContainer);
  };

  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
  };

  useEffect(() => {
    emailInput.focus();
  }, []);

  return (
    <>
      <StatusBar barStyle="light-content" />

      <ImageBackgroundStyled
        source={image}
        resizeMode="cover"
        style={styles.image}
        blurRadius={5}
      >
        <SafeAreaViewStyled>
          <LoginContainerStyled style={{ height: loginContainerHeight }}>
            <GestureRecognizer
              onSwipeUp={(state) => onSwipeUp(state)}
              onSwipeDown={(state) => onSwipeDown(state)}
              config={config}
              style={{
                height: verticalScale(410),
              }}
            >
              <TitleContainerStyled>
                <GestureRecognizer
                  onSwipeUp={(state) => onSwipeUp(state)}
                  onSwipeDown={(state) => onSwipeDown(state)}
                  config={config}
                  style={{
                    height: verticalScale(410),
                  }}
                >
                  <TitleTextStyled>Welcome</TitleTextStyled>
                </GestureRecognizer>
              </TitleContainerStyled>
              <FormInfoInputsContainerStyled>
                <FormInputStyled
                  value={email}
                  mode={"flat"}
                  placeholder="Email"
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                  onChangeText={(text) => setEmail(text)}
                  blurOnSubmit={false}
                  ref={(input) => {
                    emailInput = input;
                  }}
                  onSubmitEditing={() => {
                    const result = validateEmail();
                    console.log(result);
                    passwordInput.focus();
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                  contextMenuHidden={true}
                  enablesReturnKeyAutomatically={true}
                  keyboardType={"email-address"}
                />
                <FormInputStyled
                  placeholder="Password"
                  value={password}
                  secureTextEntry={true}
                  mode={"flat"}
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                  onChangeText={(text) => setPassword(text)}
                  blurOnSubmit={false}
                  ref={(input) => {
                    passwordInput = input;
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    setLoginContainerHeight(smallContainer);
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                />
              </FormInfoInputsContainerStyled>
              <LoginButtonContainer>
                {errorMessage && (
                  <ErrorMessageStyled>{errorMessage}</ErrorMessageStyled>
                )}
                <TouchableOpacity>
                  <LoginButtonStyled
                    mode="contained"
                    onPress={() => submitLoginInfo()}
                  >
                    Login
                  </LoginButtonStyled>
                </TouchableOpacity>
              </LoginButtonContainer>
              <LoginButtonContainer>
                <GoToSignUpButtonStyled
                  mode="flat"
                  onPress={() => console.log("signing up")}
                  color="black"
                  onPress={() => {
                    navigation.navigate("SignUp");
                  }}
                >
                  Create an account
                </GoToSignUpButtonStyled>
              </LoginButtonContainer>
            </GestureRecognizer>
          </LoginContainerStyled>
        </SafeAreaViewStyled>
      </ImageBackgroundStyled>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
});
