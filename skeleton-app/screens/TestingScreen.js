import React from "react";
import { StyleSheet, View } from "react-native";
import {
  PanGestureHandlerEventPayload,
  Gesture,
  GestureDetector,
  PanGesture,
  TapGesture,
  PanGestureHandler,
} from "react-native-gesture-handler";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  useAnimatedGestureHandler,
  withTiming,
} from "react-native-reanimated";

function Ball() {
  const x = useSharedValue(0);
  const y = useSharedValue(0);
  const ballSize = useSharedValue(1);
  const isPressed = useSharedValue(false);

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startX = x.value;
      ctx.startY = y.value;
      isPressed.value = true;
    },
    onActive: (event, ctx) => {
      x.value = ctx.startX + event.translationX;
      y.value = ctx.startY + event.translationY;
      ballSize.value = withTiming(3, { duration: 500 });
      isPressed.value = true;
    },
    onEnd: (_) => {
      x.value = withSpring(0);
      y.value = withSpring(0);
      ballSize.value = withTiming(1, { duration: 500 });
    },
    onFinish: (_) => {
      isPressed.value = false;
    },
  });

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        { translateX: x.value },
        { translateY: y.value },
        { scale: ballSize.value },
      ],
      backgroundColor: isPressed.value ? "red" : "green",
    };
  });

  return (
    <PanGestureHandler onGestureEvent={gestureHandler}>
      <Animated.View style={[styles.ball, animatedStyle]} />
    </PanGestureHandler>
  );
}

export default function Example() {
  return (
    <View style={styles.container}>
      <Ball />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ball: {
    width: 100,
    height: 100,
    borderRadius: 100,
    backgroundColor: "blue",
    alignSelf: "center",
  },
});
