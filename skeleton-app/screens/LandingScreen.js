import { SafeAreaView } from "react-native-safe-area-context";
import styled from "styled-components/native";
import { verticalScale, moderateScale } from "react-native-size-matters";
import React, { useEffect } from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  StatusBar,
} from "react-native";
import { Dimensions } from "react-native";
import { Button } from "react-native-paper";
import { AuthContext } from "../App";

const image = require("../assets/images/landing_screen.jpg");

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const ImageBackgroundStyled = styled(ImageBackground)`
  width: ${Dimensions.get("window").width};
  height: ${Dimensions.get("window").height};
  background-color: black;
  opacity: 1;
`;

const AuthContainer = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: ${verticalScale(4)};
  height: ${verticalScale(200)};
`;

const LoginButtonStyled = styled(Button)`
  width: ${moderateScale(340)};
  height: ${verticalScale(33)};
  background: #050405;
  box-shadow: 0px 1.5px 6px rgba(55, 65, 81, 0.08);
  border-radius: 4px;
  text-align: center;
  /* border: 1px solid #ffffff; */
  padding-top: ${verticalScale(5)};
  margin-top: 20px;
`;

const SignUpButtonStyled = styled(Button)`
  width: ${moderateScale(340)};
  height: ${verticalScale(33)};
  background: white;
  box-shadow: 0px 2px 6px rgba(55, 65, 81, 0.08);
  border-radius: 4px;
  text-align: center;
  /* border: 1px solid #ffffff; */
  padding-top: ${verticalScale(5)};
  margin-top: 20px;
`;

const GuestTextStyled = styled(Text)`
  margin-top: 20px;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: white;
`;

export default function ({ navigation }) {
  const { signUp } = React.useContext(AuthContext);
  useEffect(() => {
    signUp();
  }, []);
  return (
    <>
      <StatusBar barStyle="light-content" />
      <View style={styles.container}>
        <ImageBackgroundStyled
          source={image}
          resizeMode="cover"
          style={styles.image}
          blurRadius={5}
        >
          <SafeAreaViewStyled>
            <AuthContainer>
              <LoginButtonStyled
                mode="contained"
                onPress={() => navigation.navigate("SignIn")}
              >
                Login
              </LoginButtonStyled>
              <SignUpButtonStyled
                mode="contained"
                onPress={() => navigation.navigate("SignUp")}
                color="#f08e25"
              >
                Create an account
              </SignUpButtonStyled>
              {/* <GuestTextStyled>Guest</GuestTextStyled> */}
            </AuthContainer>
          </SafeAreaViewStyled>
        </ImageBackgroundStyled>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
  text: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
  },
});
