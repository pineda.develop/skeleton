import { SafeAreaView } from "react-native-safe-area-context";
import {
  Text,
  View,
  Keyboard,
  StatusBar,
  ImageBackground,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import styled from "styled-components/native";
import React, { useState, useEffect } from "react";
import { verticalScale, moderateScale } from "react-native-size-matters";
import { TextInput, Button } from "react-native-paper";
import BlackDotIconComponent from "../components/BlackDotIconComponent";
import { AuthContext } from "../App";
import GestureRecognizer, {
  swipeDirections,
} from "react-native-swipe-gestures";

const image = require("../assets/images/landing_screen.jpg");

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
`;

const SignUpContainerStyled = styled.View`
  background-color: white;
  position: absolute;
  bottom: 0;
  padding: 10px;
  width: 100%;
  padding-top: 30px;
  border-radius: 20px;
`;

const IconContainer = styled.View`
  margin: 0 auto;
`;

const FormInfoInputsContainerStyled = styled.View`
  padding: 10px;
`;

const FormInputStyled = styled(TextInput)`
  margin-top: 14px;
  background-color: white;
  border: 0;
`;

const SignUpButtonContainer = styled.View`
  margin-top: ${verticalScale(10)};
`;

// const SignUpButtonStyled = styled(Button)`
//   width: ${moderateScale(200)};
//   height: ${verticalScale(40)};
//   margin: 0 auto;
//   background-color: #000000;
//   padding-top: ${verticalScale(7)};
// `;

const SignInContainerStyled = styled.View`
  margin-top: ${verticalScale(20)};
`;

const SignInTextStyled = styled.Text`
  margin: 0 auto;
  font-size: 16px;
`;

const ErrorMessageStyled = styled.Text`
  margin: 0 auto;
  margin-bottom: 15px;
  color: red;
`;

const SignUpButtonStyled = styled(Button)`
  width: ${moderateScale(354)};
  height: ${verticalScale(33)};
  background: black;
  border-radius: 4px;
  text-align: center;
  /* border: 1px solid #ffffff; */
  padding-top: ${verticalScale(5)};
  margin-left: 5px;
`;

const TitleContainerStyled = styled.View`
  height: ${verticalScale(40)};
  background-color: white;
  margin-top: 15px;
`;

const TitleTextStyled = styled.Text`
  font-weight: 600;
  font-size: 36px;
  line-height: 20px;
  padding-top: 30px;
  padding-left: 10px;
`;

const ImageBackgroundStyled = styled(ImageBackground)`
  width: ${Dimensions.get("window").width};
  height: ${Dimensions.get("window").height};
  background-color: black;
  opacity: 1;
`;

export default function ({ navigation }) {
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const smallContainer = verticalScale(370);
  const fullScreenContainer = verticalScale(600);
  const [loginContainerHeight, setLoginContainerHeight] = useState(
    verticalScale(600)
  );
  const { signUp } = React.useContext(AuthContext);

  const validateEmail = () => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    }
    setErrorMessage("Please enter a valid email address");
    return false;
  };

  const submitSignUpInfo = async () => {
    // Check to see if passwords and confirm password match
    if (password !== confirmPassword) {
      setErrorMessage("Your passwords do not match");
    }

    // If the user has entered an email, password and a name then submit the information
    if (email && password && name && password == confirmPassword) {
      // Sign up the user.
      const isEmailValid = validateEmail(email);
      setErrorMessage(null);
      if (isEmailValid) {
        await signUp({ email, name, password, setErrorMessage });
      } else {
        setErrorMessage("Please enter a valid email address");
      }
    }
  };

  const onSwipeUp = (gestureState) => {
    setLoginContainerHeight(fullScreenContainer);
    emailInput.focus();
  };

  const onSwipeDown = (gestureState) => {
    Keyboard.dismiss();
    setLoginContainerHeight(smallContainer);
  };

  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
  };

  useEffect(() => {
    nameInput.focus();
  }, []);

  return (
    <>
      <StatusBar barStyle="light-content" />

      <ImageBackgroundStyled
        source={image}
        resizeMode="cover"
        style={styles.image}
        blurRadius={5}
      >
        <SafeAreaViewStyled>
          <SignUpContainerStyled style={{ height: loginContainerHeight }}>
            <GestureRecognizer
              onSwipeUp={(state) => onSwipeUp(state)}
              onSwipeDown={(state) => onSwipeDown(state)}
              config={config}
              style={{
                height: verticalScale(410),
              }}
            >
              <TitleContainerStyled>
                <GestureRecognizer
                  onSwipeUp={(state) => onSwipeUp(state)}
                  onSwipeDown={(state) => onSwipeDown(state)}
                  config={config}
                  style={{
                    height: verticalScale(410),
                  }}
                >
                  <TitleTextStyled>Sign Up</TitleTextStyled>
                </GestureRecognizer>
              </TitleContainerStyled>
              <FormInfoInputsContainerStyled>
                <FormInputStyled
                  placeholder="Name"
                  value={name}
                  mode={"flat"}
                  onChangeText={(text) => setName(text)}
                  blurOnSubmit={false}
                  onSubmitEditing={() => {
                    this.emailInput.focus();
                  }}
                  ref={(input) => {
                    nameInput = input;
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                  contextMenuHidden={true}
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                />
                <FormInputStyled
                  value={email}
                  mode={"flat"}
                  placeholder="Email"
                  onChangeText={(text) => setEmail(text)}
                  blurOnSubmit={false}
                  ref={(input) => {
                    emailInput = input;
                  }}
                  onSubmitEditing={() => {
                    const result = validateEmail();
                    console.log(result);
                    passwordInput.focus();
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                  contextMenuHidden={true}
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                />
                <FormInputStyled
                  placeholder="Password"
                  value={password}
                  secureTextEntry={true}
                  mode={"flat"}
                  onChangeText={(text) => setPassword(text)}
                  blurOnSubmit={false}
                  ref={(input) => {
                    passwordInput = input;
                  }}
                  onSubmitEditing={() => {
                    confirmPasswordInput.focus();
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                  contextMenuHidden={true}
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                />
                <FormInputStyled
                  placeholder="Confirm Password"
                  value={confirmPassword}
                  secureTextEntry={true}
                  mode={"flat"}
                  onChangeText={(text) => setConfirmPassword(text)}
                  blurOnSubmit={false}
                  ref={(input) => {
                    confirmPasswordInput = input;
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    setLoginContainerHeight(smallContainer);
                  }}
                  style={{ paddingHorizontal: 0, height: 40 }}
                  contextMenuHidden={true}
                  onFocus={() => {
                    if (loginContainerHeight !== fullScreenContainer) {
                      setLoginContainerHeight(fullScreenContainer);
                    }
                  }}
                />
              </FormInfoInputsContainerStyled>
              <SignUpButtonContainer>
                {errorMessage && (
                  <ErrorMessageStyled>{errorMessage}</ErrorMessageStyled>
                )}
                <TouchableOpacity>
                  <SignUpButtonStyled
                    mode="flat"
                    onPress={() => submitSignUpInfo()}
                    color="white"
                  >
                    Create account
                  </SignUpButtonStyled>
                </TouchableOpacity>
              </SignUpButtonContainer>
              <SignInContainerStyled>
                <SignInTextStyled
                  onPress={() => {
                    navigation.navigate("SignIn", {
                      activeSearchBar: true,
                    });
                  }}
                >
                  Login
                </SignInTextStyled>
              </SignInContainerStyled>
            </GestureRecognizer>
          </SignUpContainerStyled>
        </SafeAreaViewStyled>
      </ImageBackgroundStyled>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
});
