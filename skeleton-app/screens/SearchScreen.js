import { SafeAreaView } from "react-native-safe-area-context";
import React, { createRef, useEffect, useRef, useState } from "react";
import {
  Text,
  View,
  StatusBar,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import styled from "styled-components/native";
import { verticalScale } from "react-native-size-matters";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { api_endpoints } from "../api/endpoints";
import SearchCardListComponent from "../components/SearchCardListComponent";
import * as Location from "expo-location";
import { get } from "lodash";
import { FontAwesome5 } from "@expo/vector-icons";

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  background-color: white;
`;

const TopLevelContainerStyled = styled.View`
  flex: 1;
  background-color: transparent;
`;

const SearchBarContainerStyled = styled.View`
  background-color: white;
  padding: 0px 2px 0px 2px;
  margin-top: 8px;
`;

const SearchBarStyled = styled(TextInput)`
  background-color: #b9bfc6;
  border-radius: 5px;
  width: 98%;
  margin: 0px auto;
  height: ${verticalScale(30)};
  color: black;
  padding: 10px;
`;

const SearchResultsContainerStyled = styled.View`
  flex: 1;
  background-color: transparent;
  flex-direction: column;
`;

const ActivityIndicatorStyled = styled(ActivityIndicator)`
  flex: 1;
  margin: 0px auto;
`;

export default function ({ route, navigation }) {
  const [isLoading, setIsLoading] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [queryResults, setQueryResults] = useState(null);
  const [placeHolderText, setPlaceHolderText] = useState("Sound and Location");

  const onChangeSearch = (query) => {
    setSearchQuery(query);
  };

  const searchNearby = async () => {
    setIsLoading(true);
    setQueryResults(null);
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      console.log("Permission to access location was denied");
      return;
    }

    let location = await Location.getCurrentPositionAsync({});
    console.log("LOCATION", location);
    const latitude = get(location, "coords.latitude");
    const longitude = get(location, "coords.longitude");

    let url =
      api_endpoints.search.nearby +
      `?latitude=${latitude}&longitude=${longitude}`;
    console.log("url", url);
    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: url,
    });

    if (results) {
      setQueryResults(results);
      setIsLoading(false);
    }
    setIsLoading(false);
  };

  const searchTextInput = async () => {
    setIsLoading(true);
    setQueryResults(null);
    let url = api_endpoints.search.text_input + `?text_input=${searchQuery}`;
    console.log("url", url);

    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: url,
    });

    if (results) {
      setQueryResults(results);
      setPlaceHolderText("Sound and Location");
      setIsLoading(false);
    } else {
      setPlaceHolderText("No Results Found");
    }
    setIsLoading(false);
  };

  return (
    <SafeAreaViewStyled>
      <StatusBar barStyle={"dark-content"} />
      <TopLevelContainerStyled>
        <View
          style={{
            paddingLeft: 5,
            marginLeft: 4,
            marginBottom: 4,
            marginTop: 4,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              // Pass and merge params back to home screen
              navigation.navigate({
                name: "Home",
              });
            }}
          >
            <FontAwesome5 name="chevron-left" size={24} color="black" />
          </TouchableOpacity>
        </View>
        <SearchBarContainerStyled>
          <SearchBarStyled
            placeholder="Search"
            onChangeText={onChangeSearch}
            value={searchQuery}
            onSubmitEditing={() => searchTextInput()}
            autoFocus={true}
          />
        </SearchBarContainerStyled>
        <SearchResultsContainerStyled>
          {isLoading === true ? (
            <>
              <ActivityIndicatorStyled />
            </>
          ) : (
            <></>
          )}
          {queryResults !== null ? (
            <SearchCardListComponent
              establishments={queryResults}
              navigation={navigation}
            />
          ) : (
            <></>
          )}
          {isLoading === false && queryResults === null ? (
            <>
              <View style={{ flex: 1, alignItems: "center" }}>
                <View
                  style={{
                    opacity: 0.6,
                    alignSelf: "center",
                    marginTop: verticalScale(230),
                  }}
                >
                  <Text style={{ fontSize: 24 }}>{placeHolderText}</Text>
                </View>
              </View>
            </>
          ) : (
            <></>
          )}
        </SearchResultsContainerStyled>
      </TopLevelContainerStyled>
    </SafeAreaViewStyled>
  );
}
