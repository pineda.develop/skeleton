import { SafeAreaView } from "react-native-safe-area-context";
import { View } from "react-native";
import styled from "styled-components/native";
import React from "react";
import { verticalScale } from "react-native-size-matters";

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  background-color: white;
`;

const SplashScreenContainerStyled = styled.View`
  height: ${verticalScale(400)};
  background-color: white;
  padding: 10px;
  margin-top: ${verticalScale(50)};
`;

export default function ({ navigation }) {
  return (
    <SafeAreaViewStyled>
      <SplashScreenContainerStyled></SplashScreenContainerStyled>
    </SafeAreaViewStyled>
  );
}
