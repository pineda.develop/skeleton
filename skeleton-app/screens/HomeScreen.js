import { SafeAreaView } from "react-native-safe-area-context";
import React, { useState, useEffect, useRef, useContext } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  StatusBar,
  Pressable,
  Image,
  ImageBackground,
  FlatList,
  TouchableWithoutFeedback,
} from "react-native";
import { Marker } from "react-native-maps";
import styled from "styled-components/native";
import { moderateScale, verticalScale } from "react-native-size-matters";
import { AuthContext } from "../App";
import { Ionicons } from "@expo/vector-icons";
import {
  Card,
  Button,
  Icon,
  BottomSheet,
  ListItem,
} from "react-native-elements";
import Swiper from "react-native-swiper";
import { api_endpoints } from "../api/endpoints";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import MapView from "react-native-map-clustering";
import SwipeCardComponent from "../components/SwipeCardComponent";
import EstablishmentProfileComponent from "../components/EstablishmentProfileComponent";
import { MusicPlayerContext } from "../model/MusicPlayerContext";
import { Searchbar } from "react-native-paper";
import { AntDesign } from "@expo/vector-icons";
import { get, result } from "lodash";
const image = require("../assets/images/landing_screen.jpg");

const SafeAreaViewStyled = styled.SafeAreaView`
  flex: 1;
  /* background-color: black; */
  background-color: white;
`;

const Triangle = () => {
  return <View style={[styles.triangle]} />;
};

const BASE_URI = "https://source.unsplash.com/random?sig=";
export default function ({ navigation, route }) {
  const [showingEstablishmentCard, setShowingEstablishmentCard] =
    useState(false);
  const [establishmentsWithinRange, setEstablishmentsWithinRange] =
    useState(null);
  const [establishment, setCurrentViewEstablishment] = useState(null);
  const mapRef = useRef(null);
  const { state, setTracks } = useContext(MusicPlayerContext);
  const [markerFilter, setMarkerFilter] = useState("");
  const list = [
    { title: "Favorite Location" },
    { title: "Share" },
    {
      title: "Cancel",
      containerStyle: { backgroundColor: "red" },
      titleStyle: { color: "white" },
      onPress: () => setIsVisible(false),
    },
  ];

  const defaultRegion = {
    // Manhattan, New York, New York
    latitude: 40.757009,
    longitude: -74.013543,
    latitudeDelta: 0.01,
    longitudeDelta: 0.0931,
  };

  const animateToLocation = async (latitude, longitude) => {
    if (mapRef.current) {
      const camera = {
        center: {
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.9,
        },
        pitch: 0,
        heading: 0,
        altitude: 15000,
      };
      mapRef.current.animateCamera(camera, 0);
    }
  };

  const SearchForEstablishments = async () => {
    setEstablishmentsWithinRange(null);

    // Location to search from
    const latitude = 40.757009;
    const longitude = -74.013543;

    let url = api_endpoints.search.nearby;
    // + `?latitude=${latitude}&longitude=${longitude}`;
    console.log("url", url);
    const [results, error] = await ZombieAPI({
      method: "GET",
      endpoint: url,
    });

    if (results) {
      console.log(results);
      setEstablishmentsWithinRange(results);
    }
  };

  useEffect(async () => {
    SearchForEstablishments();
    if (route.params?.establishment) {
      // Get the establishment object

      establishment_object = route.params.establishment;
      animateToLocation(
        get(establishment_object, "latitude"),
        get(establishment_object, "longitude")
      );

      setCurrentViewEstablishment(establishment_object);
      setShowingEstablishmentCard(true);
    }
  }, [route.params?.establishment]);

  return (
    <SafeAreaViewStyled>
      <StatusBar barStyle={"dark-content"} />
      <HeaderComponent
        setMarkerFilter={setMarkerFilter}
        navigation={navigation}
      />

      <MapView
        ref={mapRef}
        style={styles.map}
        initialRegion={defaultRegion}
        userInterfaceStyle={"light"}
        showsUserLocation={true}
        showsTraffic={false}
        showsBuildings={false}
        showsCompass={false}
        showsPointsOfInterest={false}
        mapType={"mutedStandard"}
      >
        {establishmentsWithinRange?.map((marker, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: get(marker, ["geometry", "location", "lat"]),
              longitude: get(marker, ["geometry", "location", "lng"]),
            }}
            title={get(marker, ["name"])}
            description={get(marker, ["vicinity"])}
            onPress={async () => {
              setCurrentViewEstablishment(marker);
              setShowingEstablishmentCard(true);
            }}
          />
        ))}
      </MapView>

      {showingEstablishmentCard ? (
        <>
          <SwipeCardComponent
            setIsSwipeCardShowing={setShowingEstablishmentCard}
          >
            <EstablishmentProfileComponent
              establishment={establishment}
              showPlayButton={false}
            />
          </SwipeCardComponent>
        </>
      ) : (
        <></>
      )}
    </SafeAreaViewStyled>
  );
}

const HeaderContainerViewStyled = styled.View`
  height: ${verticalScale(88)};
  width: 100%;
  /* background-color: burlywood; */
  padding-top: 15px;
  align-items: center;
`;

const SearchBarContainerViewStyled = styled(TouchableOpacity)`
  background-color: #b9bfc6;
  border-radius: 5px;
  box-shadow: none;
  width: 98%;
  margin: 0px auto;
  height: ${verticalScale(30)};
  flex-direction: row;
`;

const FilterContainerViewStyled = styled.View`
  height: 100px;
  width: 100%;
  /* background-color: green; */
  padding: 12px 4px 0px 6px;
  flex-direction: row;
`;

const MenuContainerViewStyled = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 13px 5px 0px 2px;
`;

const SettingsContainerViewStyled = styled.View`
  width: 10%;
  align-items: center;
  align-self: center;
  margin: 2px 4px 0px 0px;
`;

const Item = ({ title, isGenereSelected }) => (
  <View
    style={{
      backgroundColor: isGenereSelected ? "#050405" : "white",
      marginLeft: 8,
      height: verticalScale(25),
      padding: 8,
      borderRadius: 10,
      opacity: isGenereSelected ? 0.8 : 1,
    }}
  >
    <Text style={{ color: isGenereSelected ? "white" : "black", fontSize: 16 }}>
      {title}
    </Text>
  </View>
);

const HeaderComponent = ({ setMarkerFilter, navigation }) => {
  const DATA = [
    {
      id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
      title: "Dance Pop",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
      title: "Rap",
    },
    {
      id: "58694a0f-3da1-471f-bd96-145571e29d72",
      title: "Rock",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbdfedf91aa97f63",
      title: "Drill",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97asf63",
      title: "Trap Latino",
    },
    {
      id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f6as3",
      title: "Electro House",
    },
  ];
  const [genre, setGenre] = useState(DATA);
  const [isGenereSelected, setIsGenereSelected] = useState(false);

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          if (!isGenereSelected) {
            setGenre([item]);
            setIsGenereSelected(true);
            setMarkerFilter("New Jersey");
          } else {
            setIsGenereSelected(false);
            setGenre(DATA);
            setMarkerFilter("");
          }
        }}
      >
        <Item title={item.title} isGenereSelected={isGenereSelected} />
      </TouchableOpacity>
    );
  };

  const goToSearchScreen = () => {
    navigation.navigate({
      name: "SearchScreen",
      merge: true,
    });
  };

  return (
    <HeaderContainerViewStyled>
      <MenuContainerViewStyled>
        <SearchBarContainerViewStyled
          onPress={() => {
            goToSearchScreen();
          }}
        >
          <AntDesign
            name="search1"
            size={24}
            color="black"
            style={{ alignSelf: "center", marginLeft: 8 }}
          />
          <Text
            style={{
              fontSize: 16,
              alignSelf: "center",
              marginLeft: 8,
              marginTop: 2,
            }}
            onPress={() => {
              goToSearchScreen;
            }}
          >
            Search
          </Text>
        </SearchBarContainerViewStyled>
      </MenuContainerViewStyled>

      <FilterContainerViewStyled>
        <FlatList
          data={genre}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          horizontal={true}
        />
      </FilterContainerViewStyled>
    </HeaderContainerViewStyled>
  );
};

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: 30,
    borderRightWidth: 30,
    borderBottomWidth: 40,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "purple",
    transform: [{ rotate: "180deg" }],
    borderRadius: 10,
  },
});
