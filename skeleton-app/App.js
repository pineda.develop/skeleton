import "react-native-gesture-handler";
import React, { useContext, useEffect, useMemo, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { View, Image } from "react-native";
import SearchScreen from "./screens/SearchScreen";
import HomeScreen from "./screens/HomeScreen";
import SignUpScreen from "./screens/SignUpScreen";
import SignInScreen from "./screens/SignInScreen";
import SettingsScreen from "./screens/SettingsScreen";
import { getAccessToken } from "./model/UserModel";
import { UserContextProvider } from "./model/UserContext";
import { ZombieAPI } from "./api/ZombieBackendAPI";
import { api_endpoints } from "./api/endpoints";
import { navigationRef } from "./RootNavigation";
import { clearAll, storeObjectData } from "./model/LocalStorage";
import { get } from "lodash";
import LandingScreen from "./screens/LandingScreen";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { verticalScale } from "react-native-size-matters";
import { AntDesign } from "@expo/vector-icons";
import HomeHeaderComponent from "./components/HomeHeaderComponent";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import TestingScreen from "./screens/TestingScreen";
import { MusicPlayerContextProvider } from "./model/MusicPlayerContext";
import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";

const Stack = createNativeStackNavigator();
export const AuthContext = React.createContext();

const Tab = createBottomTabNavigator();

function AuthTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="LandingScreen"
        component={LandingScreen}
        options={{
          tabBarStyle: { display: "none" },
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="SignUp"
        component={SignUpScreen}
        options={{
          tabBarStyle: { display: "none" },
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="SignIn"
        component={SignInScreen}
        options={{
          tabBarStyle: { display: "none" },
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
}

function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let icon;
          if (route.name === "Search") {
            icon = focused ? (
              // <AntDesign name="home" size={30} color="black" />
              <FontAwesome5 name="map-pin" size={30} color="black" />
            ) : (
              // <AntDesign name="home" size={28} color="#E5E8EA" />
              <FontAwesome5 name="map-pin" size={28} color="#E5E8EA" />
            );
          } else if (route.name === "Home") {
            icon = focused ? (
              <AntDesign name="search1" size={30} color="black" />
            ) : (
              <AntDesign name="search1" size={28} color="#E5E8EA" />
            );
          } else if (route.name === "Settings") {
            icon = focused ? (
              <Ionicons name="settings-outline" size={30} color="black" />
            ) : (
              // <AntDesign name="user" size={30} color="black" />
              // <AntDesign name="user" size={28} color="#E5E8EA" />
              <Ionicons name="settings-outline" size={28} color="#E5E8EA" />
            );
          }

          // You can return any component that you like here!
          return icon;
        },
        tabBarActiveTintColor: "tomato",
        tabBarInactiveTintColor: "gray",
        tabBarStyle: { height: verticalScale(70) },
      })}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: true,
          headerTransparent: true,
          title: "",
        }}
      />
    </Tab.Navigator>
  );
}

export default function App() {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [userToken, setUserToken] = useState(null);

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case "RESTORE_TOKEN":
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case "SIGN_IN":
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case "SIGN_OUT":
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await getAccessToken();
        // screen will be unmounted and thrown away.
        dispatch({ type: "RESTORE_TOKEN", token: userToken });
      } catch (e) {
        console.log(
          "could not verify the usersToken from async storage in App.js"
        );
      }

      // After restoring token, we may need to validate it in production apps
      // TODO:
      // 1. Validate the token by doing an API CALL
      // ------ If token is valid then dispatch({ type: "RESTORE_TOKEN", token: userToken });
      // ------ If not then dispatch Sign Out

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
    };

    bootstrapAsync();
    console.log("finished bootstrap function");
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `SecureStore`

        const { email, password, setErrorMessage } = data;

        console.log("Starting to log in");
        // Make the request to login in the user
        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.user.signin,
          body: {
            email: data.email,
            password: data.password,
          },
        });

        // If there was an error present it
        if (error) {
          setErrorMessage(error.message);
        }

        console.log("results status code", results);
        if (results) {
          if (get(results, "status_code")) {
            setErrorMessage(results.message);
          } else {
            // Store the user to async storage
            storeObjectData("solo_user", results);
            console.log("User signed in - new local storage", results);
            dispatch({ type: "SIGN_IN", token: results.tokens.access_token });
          }
        }
      },
      signOut: () => {
        //  Clear all the user data from  local storage
        clearAll();
        dispatch({ type: "SIGN_OUT" });
      },
      signUp: async (data) => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token
        clearAll();

        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.user.create_an_account,
        });

        console.log("error results", error);
        if (error) {
          setErrorMessage(error.message);
        }

        if (results) {
          // Store the user to async storage
          storeObjectData("solo_user", results);
          dispatch({ type: "SIGN_IN", token: results.tokens.access_token });
        }
      },
    }),
    []
  );

  const _cacheResourcesAsync = async () => {
    const images = [
      require("./assets/splash_screen.png"),
      require("./assets/test_artwork_cover.png"),
      require("./assets/images/landing_screen.jpg"),
    ];

    const cacheImages = images.map((image) => {
      return Asset.fromModule(image).downloadAsync();
    });
    return Promise.all(cacheImages);
  };

  if (state.isLoading) {
    return (
      <View style={{ flex: 1 }}>
        <Image source={require("./assets/splash_screen.png")} />
      </View>
    );
  }

  return (
    <AuthContext.Provider value={authContext}>
      <UserContextProvider>
        <MusicPlayerContextProvider>
          <GestureHandlerRootView style={{ flex: 1 }}>
            <NavigationContainer ref={navigationRef}>
              <Stack.Navigator>
                {state.userToken == null ? (
                  <>
                    <Stack.Screen
                      name="Auth"
                      component={AuthTabs}
                      options={{ headerShown: false }}
                    />
                  </>
                ) : (
                  <>
                    {/* <Tab.Screen name="Testing" component={TestingScreen} /> */}
                    <Stack.Screen
                      name="Home"
                      component={HomeScreen}
                      options={{
                        headerShown: false,
                        headerTransparent: true,
                        title: "",
                      }}
                    />

                    <Stack.Screen
                      name={"SearchScreen"}
                      component={SearchScreen}
                      options={{
                        headerShown: false,
                        // headerTransparent: true,
                        // title: "",
                        // headerTitleStyle: {
                        //   color: "black",
                        // },
                        // headerBackTitleVisible: false,
                      }}
                    />

                    <Stack.Screen
                      name="Settings"
                      component={SettingsScreen}
                      options={{
                        headerShown: true,
                        headerTransparent: true,
                        title: "",
                        headerTitleStyle: {
                          color: "black",
                        },
                        headerBackTitleVisible: false,
                      }}
                    />
                  </>
                )}
              </Stack.Navigator>
            </NavigationContainer>
          </GestureHandlerRootView>
        </MusicPlayerContextProvider>
      </UserContextProvider>
    </AuthContext.Provider>
  );
}
