import AsyncStorage from "@react-native-async-storage/async-storage";

export const storeStringData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log("Error saving", e);
  }
};

export const storeObjectData = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    console.log("string object", typeof jsonValue);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    console.log("Error saving", e);
  }
};

export const getStringData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return value;
  } catch (e) {
    // error reading value
    console.log("error reading value", e);
  }
};

export const getObjectData = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
    console.log("error reading value", e);
  }
};

export const removeValue = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
    return "Done";
  } catch (e) {
    // remove error
    console.log("Error removing value", e);
  }
};

export const getAllKeys = async () => {
  let keys = [];
  try {
    keys = await AsyncStorage.getAllKeys();
    return keys;
  } catch (e) {
    // read key error
    console.log("Error reading all keys", e);
  }

  // example console.log result:
  // ['@MyApp_user', '@MyApp_key']
};

export const getMultiple = async (array_of_keys) => {
  let values;
  try {
    values = await AsyncStorage.multiGet(array_of_keys);
    return values;
  } catch (e) {
    // read error
    console.log("Error getting multiple values", e);
  }

  // example console.log output:
  // [ ['@MyApp_user', 'myUserValue'], ['@MyApp_key', 'myKeyValue'] ]
};

export const clearAll = async () => {
  try {
    await AsyncStorage.clear();
    return "Done, Cleared All Data in Storage";
  } catch (e) {
    console.log(e);
    console.log("Trouble deleting all data from storage");
  }
};

export const multiSetObjects = async (key_pairs, value_pairs) => {
  try {
    await AsyncStorage.multiSet([key_pairs, value_pairs]);
  } catch (e) {
    console.log("error multi setting storage");
  }
};
