import React, { createContext, useContext, useEffect, useState } from "react";
import { getUserInfo } from "./UserModel";

export const UserContext = createContext();
export const useUser = () => useContext(UserContext);

export const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState();

  useEffect(async () => {
    const userInfo = await getUserInfo();
    if (userInfo) {
      setUser(userInfo);
    }
  }, []);

  return (
    <UserContext.Provider value={{ user }}>{children}</UserContext.Provider>
  );
};
