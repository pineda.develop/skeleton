import { api_endpoints } from "../api/endpoints";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { getObjectData, getAllKeys, removeValue } from "./LocalStorage";
import { get } from "lodash";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const getUserInfo = async () => {
  // get Data from Storage

  try {
    const getting_data = await getObjectData("solo_user");
    const user = await getting_data;
    // console.log("USER IS HERE", user);
    return user;
  } catch (error) {
    console.log(error);
  }
};

export const getUser = async () => {
  //Get user info
  try {
    const user = await getUserInfo();
    if (user) {
      return user;
    } else {
      console.log("Error");
    }
  } catch (error) {
    console.log(error);
  }
};

export const getUserTokens = async () => {
  // Get user tokens from user info dict
  const userInfo = await getUserInfo();
  console.log("user info", userInfo);
  return get(userInfo, "tokens");
};

export const getAccessToken = async () => {
  console.log("Getting User info");
  const userInfo = await getUserInfo();
  return get(userInfo, "tokens.access_token");
};

export const refreshToken = async (detail = {}) => {
  // Get the response detail
  if (detail.code === "REFRESH_TOKEN_INVALID") {
    logout();
    return false;
  }

  // Get the user refresh token from Local Storage
  const userInfo = await getUserInfo();
  console.log("BEFORE USER INFO", userInfo);
  const refreshToken = userInfo.tokens.refresh_token || null;

  // If there is no refresh token in Local Storage for the user then logout
  if (refreshToken === null) {
    logout();
    return false;
  }

  // Generate a new access token
  const [newAccessToken, error] = await ZombieAPI({
    endpoint: api_endpoints.user.refresh_tokens,
    headers: {
      Authorization: `Bearer ${refreshToken}`,
    },
  });

  console.log("old token", userInfo.tokens.access_token);
  console.log("NEW ACCESS TOKEN", newAccessToken);
  userInfo.tokens.access_token = newAccessToken;
  console.log(" UPDATED", userInfo);
  try {
    const jsonValue = JSON.stringify(userInfo);
    await AsyncStorage.setItem("solo_user", jsonValue);
    return true;
  } catch (e) {
    console.log("Error saving", e);
    return false;
  }
};

export const logout = async () => {
  console.log("Logging out...");
  const keys = await getAllKeys();
  console.log("keys", keys);
  const loggedOut = await removeValue("solo_user");
  console.log("value", loggedOut);
};
