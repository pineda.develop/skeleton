import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useReducer,
  useState,
} from "react";
import { getUserInfo } from "./UserModel";
import { useImmer } from "use-immer";
import { ZombieAPI } from "../api/ZombieBackendAPI";
import { api_endpoints } from "../api/endpoints";
import MusicPlayer from "../components/MusicPlayer";
import { get } from "lodash";
import { getObjectData, storeObjectData } from "./LocalStorage";

export const MusicPlayerContext = createContext({});

export const MusicPlayerContextProvider = ({ children }) => {
  const [musicPlayerContextValue, updateMusicPlayerContextValue] = useImmer({
    bannerInfo: {
      establishmentName: "Somewher",
      establishmentAddress: "180",
    },
  });
  const [isPlayerVisible, setIsPlayerVisible] = useState(true);
  const [currentTrack, setCurrentTrack] = useState(
    getObjectData("musicPlayer")
  );
  const [currentTrackSaveStatus, setCurrentTrackSaveStatus] = useState(false);
  const [saveTrackFunc, setSaveTrackFunc] = useState();

  const [currentEstablishment, setCurrentEstablishment] = useState({});

  const [musicPlayerState, dispatchMusicPlayerState] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case "PLAY_SONG":
          return {
            ...prevState,
            trackURI: action.trackURI,
            track: action.track,
            isLoading: false,
          };
        case "SET_TRACKS":
          return {
            ...prevState,
            trackURI: action.trackURI,
            trackList: action.trackList,
            isLoading: false,
          };
      }
    },
    {
      isLoading: true,
      trackURI: null,
      track: currentTrack,
      isPlayerVisible: true,
    }
  );

  const PlayerControlsContext = useMemo(
    () => ({
      playSong: async (data) => {
        const { trackURI, track } = data;

        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.spotify.play,
          body: {
            track_id: trackURI,
          },
        });

        if (results) {
          if (results.status === "PLAYING_SONG") {
            setIsPlayerVisible(true);
            setCurrentTrack(track);
            updateMusicPlayerContextValue((draftState) => {
              draftState.track = track;
            });
          }
        }

        dispatchMusicPlayerState({
          type: "PLAY_SONG",
          trackURI: trackURI,
          track: track,
        });
      },
      pauseSong: async (data) => {
        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.spotify.pause,
        });

        if (results) {
          const paused_status_codes = ["NO_SONG_PLAYING", "PLAYER_PAUSED"];
          if (paused_status_codes.includes(results.status)) {
            console.log("playerPaused");
          }
        }
      },
      favoriteSong: async (data) => {
        const { trackId } = data;
        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.spotify.favorite,
          body: {
            track_id: trackId,
            update_type: "favorite",
          },
        });

        if (results) {
          if (results.status === "TRACK_UPDATED") {
            setIsSongSaved(true);
          }
        }
      },
      unFavoriteSong: async (data) => {
        const { trackId } = data;
        const [results, error] = await ZombieAPI({
          method: "POST",
          endpoint: api_endpoints.spotify.unfavorite,
          body: {
            track_id: trackId,
            update_type: "unfavorite",
          },
        });

        if (results) {
          if (results.status === "TRACK_UPDATED") {
            setIsSongSaved(false);
          }
        }
      },
      setTracks: async (data) => {
        const { playlistId } = data;
        console.log("playlistID in memo is ", playlistId);
        const url =
          api_endpoints.spotify.a_playlist + "?playlist_id=" + playlistId;

        const [results, error] = await ZombieAPI({
          method: "GET",
          endpoint: url,
        });

        if (results) {
          dispatchMusicPlayerState({
            type: "SET_TRACKS",
            track: results.tracks[0],
            trackList: results.tracks,
          });
        }
      },
      syncSaveControls: (data) => {
        const { isSavedTrack, setSavedTrack } = data;
        setCurrentTrackSaveStatus(isSavedTrack);
        setSaveTrackFunc(setSavedTrack);
      },
      hidePlayer: () => {
        setIsPlayerVisible(false);
      },
    }),
    []
  );

  useEffect(() => {
    if (get(musicPlayerState, "isPlayerVisible")) {
      console.log("trying");
      setIsPlayerVisible(true);
    }
  }, [musicPlayerState]);

  return (
    <MusicPlayerContext.Provider
      value={{
        ...musicPlayerContextValue,
        ...PlayerControlsContext,
        musicPlayerState,
        isPlayerVisible,
        setIsPlayerVisible,
        setCurrentEstablishment,
        saveTrackFunc,
        setSaveTrackFunc,
        currentTrackSaveStatus,
        setCurrentTrackSaveStatus,
      }}
    >
      {children}
      {isPlayerVisible && musicPlayerContextValue.track ? (
        <MusicPlayer
          track={currentTrack}
          establishment={currentEstablishment}
        />
      ) : (
        <></>
      )}
    </MusicPlayerContext.Provider>
  );
};
