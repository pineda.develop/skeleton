import { get } from "lodash";
import { getAccessToken, getUserInfo, refreshToken } from "../model/UserModel";
import { api_endpoints } from "./endpoints";

export const ZombieAPI = async (config) => {
  const { endpoint, method = "GET", body, headers } = config;

  try {
    // Create the header requirements for the api request
    const accessToken = await getAccessToken();
    let fullHeaders = {
      "Content-Type": "application/json",
    };
    if (accessToken) fullHeaders.Authorization = `Bearer ${accessToken}`;
    fullHeaders = { ...fullHeaders, ...headers };

    //  Make the API call to the backend
    const result = await fetch(`${api_endpoints.domain.backend}${endpoint}`, {
      method: method.toUpperCase(),
      body: body && JSON.stringify(body),
      headers: fullHeaders,
    });

    const data = await result.json();
    const status_code = get(data, "status_code");

    // Check to see if the token is expired
    if (status_code == 401) {
      const success = await refreshToken(result.detail);
      if (success) {
        return ZombieAPI(config);
      }
    }

    // If there is any other status code
    if (status_code) {
      return [undefined, data];
    }

    return [data, undefined];
  } catch (err) {
    return { Error: "Something went wrong", message: err };
  }
};
