// List of endpoints for the backend
// The reason for this is if we update one value here then the entire codebase gets updated
// with the new endpoint instead of having to update one by one
export const api_endpoints = {
  // domain: {
  //   backend: "https://soundandlocation.com/api/v1",
  //   oauth: "https://soundandlocation.com/oauth",
  // },
  domain: {
    backend: "https://sound-and-location.ngrok.io/api/v1",
    oauth: "https://sound-and-location.ngrok.io/oauth",
  },
  user: {
    create_an_account: "/user/signup",
    signin: "/user/signin",
    check_tokens: "/user/check",
    refresh_tokens: "/user/tokens/refresh",
  },
  spotify: {
    get_spotify_url: "/spotify/get-spotify-url",
    check_for_account: "/spotify/check",
    delete_account: "/spotify/delete-account",
    play: "/spotify/play",
    pause: "/spotify/pause",
    favorite: "/spotify/favorite-track",
    unfavorite: "/spotify/unfavorite-track",
    profile: "/spotify/profile",
    playlists: "/spotify/get-all-playlists",
    get_playlist: "/spotify/get-a-playlist",
    get_saved_tracks: "/spotify/get-saved-tracks",
  },
  search: {
    establishments: "/search/establishments",
    nearby: "/search/nearby",
    text_input: "/search/text-input",
  },
};
